var blocuser = require("./src/blocuser");
var worldscan = require("./src/worldscan");

if (typeof(process.argv[2]) === "undefined") {
	console.log("No command specified.\nUsage: iojs " + process.argv[1].split("\\")[process.argv[1].split("\\").length - 1] + " <Command>.\nUse help for more information");
} else {
	switch(process.argv[2].toLowerCase()) {
		case "userscan":
			if (isNaN(parseInt(process.argv[3]))) {
				console.log("Please provide a numeric ID to query")
			} else {
				console.log("Querying ID " + process.argv[3] + "...")
				blocuser.parse(process.argv[3], function(BlocUser) {
					console.log(BlocUser);
				});
			}
			break;
		case "help":
			console.log("Commands: \nuserscan <ID> - Get information about the User with the specified ID\nworldscan [filter] - Scan entire world. Optionally add a filter.\n\nAvailable filters:\nuranium - Scan for uranium");
			break;
		case "worldscan":
			worldscan.getLastPage(function(maxPage) {
				for(i = 1; i <= maxPage; i++) {
					worldscan.loadPage(i, function(value) {
						blocuser.parse(value, function(BlocUser, page) {
							if (typeof(process.argv[3]) === "undefined") {
								console.log("Nation " + BlocUser.general.name + " (ID: " + BlocUser.general.id + ") is in the alliance " + BlocUser.foreign.alliance.name + ". The army size is " + BlocUser.military.size + " and the person is offline for " + BlocUser.general.lastOnline + " hours.");
							} else if (process.argv[3].toLowerCase() === "uranium") {
								if (BlocUser.economy.uranium > 0) {
									console.log("Nation " + BlocUser.general.name + " (ID: " + BlocUser.general.id + ") has " + BlocUser.economy.uranium + " Tons of Uranium! " + BlocUser.general.name + " is in the alliance " + BlocUser.foreign.alliance.name + ". The army size is " + BlocUser.military.size + " and the person is offline for " + BlocUser.general.lastOnline + " hours.");
								}
							}
						});
					});
				}
			});
			break;
		case "version":
			console.log("v1.0.0");
			break;
		default:
			console.log("Command not recognized. Use help to get a list of commands");
			break;
	}
}