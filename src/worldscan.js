/* 
 * 
 * Worldscanner Module
 * This module contains functions to query the World Ranking for nations with different filters
 * 
*/

var enums = require('./enums');
var request = require('request');
var jsdom = require('jsdom');
var blocuser = require('./blocuser');
var jquery = require('fs').readFileSync('jquery.js').toString();

var endpoint = "http://blocgame.com/rankings.php?page=";

function getNationsOnPage($) {
	tmp = [];
	$("tr").each(function (index) {
		if (index > 0) {
			userid = $(this).find("h4").find("a").attr("href");
			if (typeof(userid) !== "undefined") {
				tmp.push(userid.replace("stats.php?id=", ""))
			}
		}
	});
	return tmp;
}

function loadPage(page, callback) {
	if (typeof(callback) !== 'function') { callback = function(blocusr){}; }
	request(endpoint + page, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			jsdom.env({
				html: body,
				src: [jquery],
				done: function (errors, window) {
					if (errors) { console.log(errors); }
					window.$.each(getNationsOnPage(window.$), function(index, value) {
						callback(value, page);
					});
				}
			});
		}
	});
}

function getLastPage(callback) {
	if (typeof(callback) !== 'function') { callback = function(lastpage){}; }
	request(endpoint + "1", function (error, response, body) {
		if (!error && response.statusCode == 200) {
			jsdom.env({
				html: body,
				src: [jquery],
				done: function (errors, window) {
					if (errors) { console.log(errors); }
					$ = window.$;
					callback(parseInt($($(".pagination").find("li")[8]).find("a").attr("href").replace("rankings.php?page=", "")));
				}
			});
		}
	});
}

module.exports.loadPage = loadPage;
module.exports.getNationsFromHTML = getNationsOnPage;
module.exports.getLastPage = getLastPage;