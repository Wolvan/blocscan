var exports = module.exports;

exports.approval = {
	EnemyOfThePeople: -4,
	UtterlyDespised: -3,
	Hated: -2,
	Disliked: -1,
	Middling: 0,
	Decent: 1,
	Liked: 2,
	Loved: 3,
	Adored: 4,
	WorshipedAsAGod: 5
};

exports.politicalSystems = {
	Dictatorship: -2,
	MilitaryJunta: -1,
	OnePartyState: 0,
	AuthoritarianDemocracy: 1,
	LiberalDemocracy: 2
};

exports.stability = {
	BrinkOfCollapse: -4,
	MassProtests: -3,
	Rioting: -2,
	Chaotic: -1,
	RisingTension: 0,
	SeeminglyCalm: 1,
	Quiet: 2,
	VeryStable: 3,
	Entrenched: 4,
	Unsinkable: 5
};

exports.QoL = {
	Crisis: -5,
	Disastrous: -4,
	Desperate: -3,
	Impoverished: -2,
	Poor: -1,
	Average: 0,
	AboveAverage: 1,
	Decent: 2,
	Good: 3,
	Developed: 4
};

exports.rebels = {
	None: 0,
	ScatteredTerrorists: -1,
	Guerrillas: -2,
	OpenRebellion: -3,
	CivilWar: -4
}

exports.economicSystems = {
	CentralPlanning: -1,
	MixedEconomy: 0,
	FreeMarket: 1
}

exports.alignment = {
	East: -1,
	None: 0,
	West: 1
}

exports.reputation = {
	AxisOfEvil: -5,
	MadDog: -4,
	Pariah: -3,
	Isolated: -2,
	Questionable: -1,
	Normal: 0,
	Good: 1,
	Nice: 2,
	Angelic: 3,
	GhandiLike: 4
}

exports.manpower = {
	Depleted: -3,
	NearDepletion: -2,
	Low: -1,
	Halved: 0,
	Plentiful: 1,
	Untapped: 2
}

exports.tech = {
	FinestOf19thCentury: 0,
	WW1Surplus: 1,
	WW2Surplus: 2,
	WWKSurplus: 3,
	WWVSurplus: 4,
	AlmostModern: 5,
	PersianGulf: 6,
	Advanced: 7
}

exports.training = {
	UndisciplinedRabble: -2,
	Poor: -1,
	Standard: 0,
	Good: 1,
	Elite: 2
}

exports.airforce = {
	None: 0,
	Meagre: 1,
	Small: 2,
	Mediocre: 3,
	SomewhatLarge: 4,
	Large: 5,
	Powerful: 6,
	VeryPowerful: 7
}