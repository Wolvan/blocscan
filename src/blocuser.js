/* 
 * 
 * Blocuser Module
 * This model contains the BlocUser class as well as functions to load and parse a Bloc User by ID
 * 
*/ 

var enums = require('./enums');
var request = require('request');
var jsdom = require('jsdom');
var jquery = require('fs').readFileSync('jquery.js').toString();

var endpoint = "http://blocgame.com/stats.php?id=";

blocuser = function(obj) {
	if (typeof(obj) === "undefined") { obj = {}; }
	this.general = {}; this.government = {}; this.economy = {}; this.foreign = {}; this.military = {};
	Object.freeze(this);
	this.general.id = obj.id;
	this.general.name = obj.name;
	this.general.username = obj.username;
	this.general.lastOnline = obj.lastOnline;
	this.general.approval = obj.approval;
	this.government.system = obj.psystem;
	this.government.stability = obj.stability;
	this.government.QoL = obj.QoL;
	this.government.territory = obj.territory;
	this.government.rebels = obj.rebels;
	this.economy.system = obj.esystem;
	this.economy.industry = obj.industry;
	this.economy.gdp = obj.gdp;
	this.economy.growth = obj.growth;
	this.economy.oilReserves = obj.reserves;
	this.economy.oilProduction = obj.oproduction;
	this.economy.materialProduction = obj.mproduction;
	this.economy.uranium = obj.uranium;
	this.foreign.alignment = obj.alignment;
	this.foreign.region = obj.region;
	this.foreign.alliance = {
		id: obj.allianceId, name: obj.allianceName
	};
	this.foreign.reputation = obj.reputation;
	this.military.size = obj.army;
	this.military.manpower = obj.manpower;
	this.military.tech = obj.tech;
	this.military.training = obj.training;
	this.military.airforce = obj.airforce;
	this.military.navy = obj.navy;
	this.military.wars = {
		attack: obj.attack,
		defend: obj.defend
	}
}

function loadBlocUser(id, callback) {
	if (typeof(callback) !== 'function') { callback = function(blocusr){}; }
	request(endpoint + id, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			jsdom.env({
				html: body,
				src: [jquery],
				done: function (errors, window) {
					parseBlocUser(window.$, id, callback);
				}
			});
		}
	});
}

function parseBlocUser($, id, callback) {
	if (typeof(callback) !== 'function') { callback = function(blocusr){}; }
	obj = {};
	obj.id = parseInt(id);
	obj.name = $("#nationtitle").text()
	obj.username = $($(".lead")[1]).find("b").text();
	lastonline = $("font").filter(function() { return parseInt($(this).attr('size')) == "2"; }).text();
	if (lastonline === "online now") {
		obj.lastOnline = 0;
	} else {
		obj.lastOnline = parseInt(lastonline.replace("last online ", "").replace(" hours ago"));
	}
	obj.uranium = 0;
	$("tr").each(function(index){
		key = $($(this).find("td")[0]).text();
		value = $($(this).find("td")[1]).text();
		switch(key) {
			case "Approval:":
				switch(value) {
					case "Enemy of the People":
						obj.approval = enums.approval.EnemyOfThePeople;
						break;
					case "Utterly Despised":
						obj.approval = enums.approval.UtterlyDespised;
						break;
					case "Hated":
						obj.approval = enums.approval.Hated;
						break;
					case "Disliked":
						obj.approval = enums.approval.Disliked;
						break;
					case "Middling":
						obj.approval = enums.approval.Middling;
						break;
					case "Decent":
						obj.approval = enums.approval.Decent;
						break;
					case "Liked":
						obj.approval = enums.approval.Liked;
						break;
					case "Loved":
						obj.approval = enums.approval.Loved;
						break;
					case "Adored":
						obj.approval = enums.approval.Adored;
						break;
					case "Worshiped as a God":
						obj.approval = enums.approval.WorshipedAsAGod;
						break;
				}
				break;
			case "Political System:":
				switch(value) {
					case "Dictatorship":
						obj.psystem = enums.politicalSystems.Dictatorship;
						break;
					case "Military Junta ":
						obj.psystem = enums.politicalSystems.MilitaryJunta;
						break;
					case "One Party State":
						obj.psystem = enums.politicalSystems.OnePartyState;
						break;
					case "Authoritarian Democracy":
						obj.psystem = enums.politicalSystems.AuthoritarianDemocracy;
						break;
					case "Liberal Democracy ":
						obj.psystem = enums.politicalSystems.LiberalDemocracy;
						break;
				}
				break;
			case "Stability:":
				switch(value) {
					case "Brink of Collapse":
						obj.stability = enums.stability.BrinkOfCollapse;
						break;
					case "Mass Protests":
						obj.stability = enums.stability.MassProtests;
						break;
					case "Rioting":
						obj.stability = enums.stability.Rioting;
						break;
					case "Chaotic":
						obj.stability = enums.stability.Chaotic;
						break;
					case "Rising Tensions":
						obj.stability = enums.stability.RisingTension;
						break;
					case "Seemingly Calm":
						obj.stability = enums.stability.SeeminglyCalm;
						break;
					case "Quiet":
						obj.stability = enums.stability.Quiet;
						break;
					case "Very Stable":
						obj.stability = enums.stability.VeryStable;
						break;
					case "Entrenched":
						obj.stability = enums.stability.Entrenched;
						break;
					case "Unsinkable":
						obj.stability = enums.stability.Unsinkable;
						break;
				}
				break;
			case "Quality of Life:":
				switch(value) {
					case "Humanitarian Crisis":
						obj.QoL = enums.QoL.Crisis;
						break;
					case "Disastrous":
						obj.QoL = enums.QoL.Disastrous;
						break;
					case "Desperate":
						obj.QoL = enums.QoL.Desperate;
						break;
					case "Impoverished":
						obj.QoL = enums.QoL.Impoverished;
						break;
					case "Poor":
						obj.QoL = enums.QoL.Poor;
						break;
					case "Average":
						obj.QoL = enums.QoL.Average;
						break;
					case "Above Average":
						obj.QoL = enums.QoL.AboveAverage;
						break;
					case "Decent":
						obj.QoL = enums.QoL.Decent;
						break;
					case "Good":
						obj.QoL = enums.QoL.Good;
						break;
					case "Developed":
						obj.QoL = enums.QoL.Developed;
						break;
				}
				break;
			case "Territory:":
				obj.territory = parseInt(value.replace(" km2"));
				break;
			case "Rebel Threat:":
				switch(value) {
					case "None":
						obj.rebels = enums.rebels.None;
						break;
					case "Scattered Terrorists":
						obj.rebels = enums.rebels.ScatteredTerrorists;
						break;
					case "Guerrillas":
						obj.rebels = enums.rebels.Guerrillas;
						break;
					case "Open Rebellion":
						obj.rebels = enums.rebels.OpenRebellion;
						break;
					case "Civil War":
						obj.rebels = enums.rebels.CivilWar;
						break;
				}
				break;
			case "Economic System:":
				switch(value) {
					case "Central Planning":
						obj.esystem = enums.economicSystems.CentralPlanning;
						break;
					case "Free Market":
						obj.esystem = enums.economicSystems.MixedEconomy;
						break;
					case "Mixed Economy":
						obj.esystem = enums.economicSystems.FreeMarket;
						break;
				}
				break;
			case "Industry:":
				if (value === "None") { obj.industry = 0; } else {
					obj.industry = parseInt(value.replace(" factories").replace(" factory"));
				}
				break;
			case "Gross Domestic Product:":
				obj.gdp = parseInt(value.replace(" million", "").replace("$", ""));
				break;
			case "Growth:":
				obj.growth = parseInt(value.replace(" million per month", "").replace("$", ""));
				break;
			case "Discovered Oil Reserves:":
				obj.reserves = parseInt(value.replace(" Mbbl"));
				break;
			case "Oil Production:":
				obj.oproduction = parseInt(value.replace(" Mbbl per month"));
				break;
			case "Raw Material Production:":
				obj.mproduction = parseInt(value.replace(" Hundred Tons per month"));
				break;
			case "Uranium:":
				obj.uranium = parseInt(value.replace(" Tons"));
			case "Official Alignment:":
				switch(value) {
					case "Soviet Union ":
						obj.alignment = enums.alignment.East;
						break;
					case "Neutral ":
						obj.alignment = enums.alignment.None;
						break;
					case "United States ":
						obj.alignment = enums.alignment.West;
						break;
				}
				break;
			case "Region:":
				obj.region = value;
				break;
			case "Alliance:":
				obj.allianceName = value;
				if (value !== "None") { obj.allianceId = parseInt($($(this).find("td")[1]).find("a").attr("href").replace("alliancestats.php?allianceid=", "")); } else { obj.allianceId = 0; }
				break;
			case "Reputation:":
				switch(value) {
					case "Axis of Evil":
						obj.reputation = enums.reputation.AxisOfEvil;
						break;
					case "Mad Dog":
						obj.reputation = enums.reputation.MadDog;
						break;
					case "Pariah":
						obj.reputation = enums.reputation.Pariah;
						break;
					case "Isolated":
						obj.reputation = enums.reputation.Isolated;
						break;
					case "Questionable":
						obj.reputation = enums.reputation.Questionable;
						break;
					case "Normal":
						obj.reputation = enums.reputation.Normal;
						break;
					case "Good":
						obj.reputation = enums.reputation.Good;
						break;
					case "Nice":
						obj.reputation = enums.reputation.Nice;
						break;
					case "Angelic":
						obj.reputation = enums.reputation.Angelic;
						break;
					case "Gandhi-like":
						obj.reputation = enums.reputation.GhandiLike;
						break;
				}
				break;
			case "Army Size:":
				obj.army = parseInt(value.replace("k active personnel"));
				break;
			case "Manpower:":
				switch(value) {
					case "Depleted":
						obj.manpower = enums.manpower.Depleted;
						break;
					case "Near Depletion":
						obj.manpower = enums.manpower.NearDepletion;
						break;
					case "Low":
						obj.manpower = enums.manpower.Low;
						break;
					case "Halved":
						obj.manpower = enums.manpower.Halved;
						break;
					case "Plentiful":
						obj.manpower = enums.manpower.Plentiful;
						break;
					case "Untapped":
						obj.manpower = enums.manpower.Untapped;
						break;
				}
				break;
			case "Equipment:":
				switch(value) {
					case "Finest Of The 19th Century":
						obj.tech = enums.tech.FinestOf19thCentury;
						break;
					case "First World War surplus":
						obj.tech = enums.tech.WW1Surplus;
						break;
					case "Second World War surplus":
						obj.tech = enums.tech.WW2Surplus;
						break;
					case "Korean World War surplus":
						obj.tech = enums.tech.WWKSurplus;
						break;
					case "Vietnam World War surplus":
						obj.tech = enums.tech.WWVSurplus;
						break;
					case "Almost Modern":
						obj.tech = enums.tech.AlmostModern;
						break;
					case "Persian Gulf World War surplus":
						obj.tech = enums.tech.PersianGulf;
						break;
					case "Advanced":
						obj.tech = enums.tech.Advanced;
						break;
				}
				break;
			case "Training:":
				switch(value) {
					case "Undisciplined Rabble":
						obj.training = enums.training.UndisciplinedRabble;
						break;
					case "Poor":
						obj.training = enums.training.Poor;
						break;
					case "Standard":
						obj.training = enums.training.Standard;
						break;
					case "Good":
						obj.training = enums.training.Good;
						break;
					case "Elite":
						obj.training = enums.training.Elite;
						break;
				}
				break;
			case "Airforce:":
				switch(value) {
					case "None":
						obj.airforce = enums.airforce.None;
						break;
					case "Meagre":
						obj.airforce = enums.airforce.Meagre;
						break;
					case "Small":
						obj.airforce = enums.airforce.Small;
						break;
					case "Mediocre":
						obj.airforce = enums.airforce.Mediocre;
						break;
					case "Somewhat Large":
						obj.airforce = enums.airforce.SomewhatLarge;
						break;
					case "Large":
						obj.airforce = enums.airforce.Large;
						break;
					case "Powerful":
						obj.airforce = enums.airforce.Powerful;
						break;
					case "Very Powerful":
						obj.airforce = enums.airforce.VeryPowerful;
						break;
				}
				break;
			case "Navy:":
				res = value.split("\n");
				obj.navy = parseInt(res[res.length - 1].replace(" ships ", "").replace(" ship ", ""));
				break;
			case "Wars:":
				if (value === "At peace.") {
					obj.attack = 0;
					obj.defend = 0;
				}
			default:
				break;
		}
	});
	callback(new blocuser(obj));
}

/*
 * 
 * Export all functions and variables for use by the Main JS File
 * 
*/
module.exports = blocuser;
module.exports.parse = loadBlocUser;